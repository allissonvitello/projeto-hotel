<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<title>Cadastro de Líderes</title>
		<style>
			th {
			  text-align: left;
			}
		</style>
	</head>
	
	<body>
		<h2>Cadastro de Líderes:</h2>
	
		<form name="formCadastro" method="post" action="salvar.jsp">
			
			<table>
				<tr>
					<th>Nome:</th>
					<th><input type="text" name="nome"></th>
				</tr>
				<tr>
					<th>CPF:</th>
					<th><input type="text" name="cpf"></th>
				</tr>
				<tr>
					<th>Data de nascimento:</th>
					<th><input type="text" name="datadenascimento"></th>
				</tr>
				<tr>
					<th>Cidade:</th>
					<th><input type="text" name="cidade"></th>
				</tr>
				<tr>
					<th>Estado:</th>
					<th><input type="text" name="estado"></th>
				</tr>
				<tr>
					<th>Telefone:</th>
					<th><input type="text" name="telefone"></th>
				</tr>
				<tr>
					<th><input type="submit" value="Enviar"></th>
				</tr>
			</table>
	
		</form>
	
	</body>
</html>