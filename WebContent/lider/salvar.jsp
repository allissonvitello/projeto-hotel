<%@page import="persistencia.LiderBD"%>
<%@page import="java.util.Date"%>
<%@page import="dominio.Lider"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>
	
	<head>
		<meta charset="UTF-8">
		<title>Cadastro de Líderes</title>
	</head>
	
	<body>
		<%
		Lider lider = new Lider();
		
		lider.setNome("nome");
		lider.setCpf(request.getParameter("cpf"));
		
		//Trecho em que obtemos a data como String e é feito um parse para Date
		String dataDeNascimento = request.getParameter("datadenascimento");
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date date = formato.parse(dataDeNascimento);
		lider.setDataNascimento(date);
				
		lider.setCidade(request.getParameter("cidade")); 
		lider.setEstado(request.getParameter("estado"));
		lider.setTelefone(request.getParameter("telefone"));
		
		LiderBD.inserir(lider);
		%>
		
		Líder cadastrado com sucesso!
		
	</body>
</html>