package dominio;

import java.util.Date;

public class Reserva {

	private Date dataCadastro;
	private Date dataReserva;
	private int quantidadeDiarias;
	private Lider lider;
	private Romaria romaria;

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataReserva() {
		return dataReserva;
	}

	public void setDataReserva(Date dataReserva) {
		this.dataReserva = dataReserva;
	}

	public int getQuantidadeDiarias() {
		return quantidadeDiarias;
	}

	public void setQuantidadeDiarias(int quantidadeDiarias) {
		this.quantidadeDiarias = quantidadeDiarias;
	}

	public Lider getLider() {
		return lider;
	}

	public void setLider(Lider lider) {
		this.lider = lider;
	}

	public Romaria getRomaria() {
		return romaria;
	}

	public void setRomaria(Romaria romaria) {
		this.romaria = romaria;
	}

}
