package persistencia;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import com.thoughtworks.xstream.XStream;

import dominio.Lider;

public class LiderBD {

	private static ArrayList<Lider> lista = new ArrayList<Lider>();

	public static void inserir(Lider lider) {

		lerXml();
		lista.add(lider);
		salvarXml();
	}

	public static void alterar(Lider lider) {
		excluir(lider.getCpf());
		inserir(lider);
	}

	public static void excluir(String cpf) {
		lerXml();
		for (int i = 0; i < lista.size(); i++) {
			Lider cadaLider = lista.get(i);
			if (cadaLider.getCpf().equals(cpf)) {
				lista.remove(i);
				break;
			}
		}
		salvarXml();
	}

	public static ArrayList<Lider> listar() {
		lerXml();
		return lista;
	}

	/**
	 * O método lerXml() vai pegar o que está no arquivo XML e trazer para o meu
	 * ArrayList.
	 */

	public static void lerXml() {
		File arquivo = new File("lideres.xml");
		if (arquivo.exists()) {
			// Armazena o XML no ArrayList
			XStream xstream = new XStream();
			xstream.alias("lider", Lider.class);
			lista = (ArrayList<Lider>) xstream.fromXML(arquivo);
		} else {
			lista = new ArrayList<Lider>();
		}
	}

	/**
	 * O método salvarXml() faz o contrário do lerXml(), ou seja, ele salva as
	 * informações do ArrayList no XML.
	 */

	public static void salvarXml() {
		XStream xstream = new XStream();
		xstream.alias("lider", Lider.class);
		try {
			FileWriter escritor = new FileWriter("lideres.xml");
			escritor.write(xstream.toXML(lista));
			escritor.close();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

}
