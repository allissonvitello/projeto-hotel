package persistencia;

import java.util.ArrayList;

import dominio.Reserva;

public class ReservaDB {
	
	private static ArrayList<Reserva> lista = new ArrayList<Reserva>();

	public static void inserir(Reserva reserva) {
		lista.add(reserva);
	}

	public static void alterar(Reserva reserva) {
		excluir(reserva.getRomaria().getCodigo());
		inserir(reserva);
	}

	public static void excluir(int codigo) {
		for (int i = 0; i < lista.size(); i++) {
			Reserva reserva = lista.get(i);
			if (reserva.getRomaria().getCodigo() == codigo) {
				lista.remove(i);
				break;
			}
		}
	}

	public static ArrayList<Reserva> listar() {
		return lista;
	}

}
