package persistencia;

import java.util.ArrayList;

import dominio.Romaria;

public class RomariaDB {

	private static ArrayList<Romaria> lista = new ArrayList<Romaria>();

	public static void inserir(Romaria romaria) {
		lista.add(romaria);
	}

	public static void alterar(Romaria romaria) {
		excluir(romaria.getCodigo());
		inserir(romaria);
	}

	public static void excluir(int codigo) {
		for (int i = 0; i < lista.size(); i++) {
			Romaria romaria = lista.get(i);
			if (romaria.getCodigo() == codigo) {
				lista.remove(i);
				break;
			}
		}
	}

	public static ArrayList<Romaria> listar() {
		return lista;
	}

}
